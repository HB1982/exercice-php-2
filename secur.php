
<?php
// (A) OUVRIR SESSION
session_start();
 
// (B) FERMER SESSION
if (isset($_POST["logout"])) { unset($_SESSION["user"]); }
 
// (C) REDIRIGER VERS OUVRIR SESSION
if (!isset($_SESSION["user"])) {
  header("Location: login.php");
  exit();
}
