<?php
// page protégée
session_start();


//deconnection
if(isset($_POST["logout"])){
  unset($_SESSION['user']);}
  

//revenir sur la page si pas de connexion

if(!isset($_SESSION['user'])){
  header("Location :login.php");
  exit();
}
?>

<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>HOME PAGE</title>
  <link rel="stylesheet" href="style2.css">
</head>
<body>
  <h1>Bienvenue <?php echo $_SESSION['user'] ?></h1>


<form method="post">
<input type="hidden" name="logout" value="1">

<a href="disconnect.php">Déconnexion</a>


</form>
</body>
</html>